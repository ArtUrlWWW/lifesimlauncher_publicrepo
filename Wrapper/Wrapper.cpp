// Wrapper.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#define _WIN32_WINNT 0x0501 //this is for XP


int main()
{
	ShowWindow(GetConsoleWindow(), SW_HIDE);

	HRSRC hrsrc = FindResource(NULL, MAKEINTRESOURCE(IDR_BINARYTYPE1), _T("BINARYTYPE"));
	//FindResource(NULL, MAKEINTRESOURCE(IDR_BINARYTYPE1), RT_BITMAP);
	HGLOBAL hLoaded = LoadResource(NULL, hrsrc);
	LPVOID lpLock = LockResource(hLoaded);
	DWORD dwSize = SizeofResource(NULL, hrsrc);
	std::wstring path = getEnvVar("TEMP") + TEXT("/LifeSimLauncher.exe");

	HANDLE hFile = CreateFile(path.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	DWORD dwByteWritten;
	WriteFile(hFile, lpLock, dwSize, &dwByteWritten, NULL);
	CloseHandle(hFile);
	FreeResource(hLoaded);

	ShellExecute(NULL, TEXT("open"), path.c_str(), NULL, NULL, SW_SHOWDEFAULT);

	return 0;
}

std::wstring getEnvVar(std::string const & key)
{
	char * val = getenv(key.c_str());
	std::wstring wstr(val, val + strlen(val));
	return wstr;
}

