#pragma once

#include <urlmon.h>
#pragma comment(lib, "urlmon.lib")
#include "pugixml.hpp"
#include <string>
#include <boost/filesystem.hpp>

class FileIO {
public:
	static long long int removeEmptyDirs(string rootFolder);
	static void removeUnNeededFiles(ResultArrays *ras);
	static void countFilesInDir(boost::filesystem::path dirn, int *fileCounter);
	static void WriteWstringToFile(wstring *stringToPrint, wstring *pathToFile, bool append);
	static void WriteStringToFile(string *stringToPrint, string *pathToFile, bool append);
	static long long int* GetFileSize(WIN32_FIND_DATA *f);
	static void hashFilesInDir(boost::filesystem::path dirn, pugi::xml_node *root,
		void(*calcHashFunction)(wstring , wstring , uintmax_t, pugi::xml_node *));


};