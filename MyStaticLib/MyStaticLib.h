#pragma once

// My headers
#include "Structs.h"
#include "Hasher.h"
#include "FileIO.h"
#include "MyStrings.h"
#include "NetIO.h"
#include "DownloadProgress.h"
#include "MyXML.h"


class MyStaticLib {
public:
	static string pathToFilesOnServerStr;
	static wstring pathToFilesOnServer;
	static wstring remoteDistribVersion;
};