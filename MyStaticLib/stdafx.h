// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers


#include <Windows.h>

//urlmon
#include <sstream>


//Other
#include <curl/curl.h>


// CryptoPP
#include "tiger.h"
#include "hex.h"
#include "files.h" 
#include "base32.h"
#include "sha.h"

#include "MyStaticLib.h"




