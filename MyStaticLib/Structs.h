#pragma once

#include <string>
#include <map>

using namespace std;

struct FileInfo {
	uintmax_t fileSize;
	wstring filePathOnServer;
	wstring filePathLocal;
	wstring fileHash;
};



struct ResultArrays
{
	long long int bytesToBeDownloaded;
	//void *currentWorkStatus;
	long long int bytesAlreadyDownloaded;
	long long int *downloadedBytes;
	//void *downloadingBytesCurrent;
};
