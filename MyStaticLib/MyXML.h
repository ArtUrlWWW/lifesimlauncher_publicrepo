#pragma once

class MyXML {
public:
	static pugi::xml_node* initLocalXmlFileList(pugi::xml_document *doc);
	static void localXmlFileListAddElement(pugi::xml_node *root, wstring *pathToFile, wstring *fileHash, wstring *fileSize);

};