#pragma once


class NetIO {
public:
	static wstring getUrlContent(string URL, bool firstAttempt);
	static bool readRemoteFileListXml(string serverHost, ResultArrays *ras);

	static map<wstring, FileInfo> *localFilesForDeletion;
	static map<wstring, FileInfo> *remoteFileForDownload;

	static int triedTimes;

	static bool GetFile(const std::string& serverName,
		const std::string& getCommand,
		std::ofstream& outFile);
};