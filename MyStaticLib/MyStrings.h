#pragma once

class MyStrings {
public:
	static wstring StringToWString(string s);
	static void StringToWStringOld(string *s, wstring *ws);
	static wchar_t* Utf8ToUnicode(string *inStr);

};