#include "stdafx.h"

//Boost
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

map<wstring, FileInfo> *Hasher::remoteXmlListHashToFile = new map<wstring, FileInfo>;

map<wstring, FileInfo> *Hasher::LocalIndexedFilesHashToFile = new map<wstring, FileInfo>;

wstring Hasher::getHashOfFile(wstring *pathToFile) {
	string *hash = new string("");

	{
		boost::this_thread::disable_interruption di;

		CryptoPP::Tiger *sha1 = new CryptoPP::Tiger;
		CryptoPP::FileSource((*pathToFile).c_str(), true,
			new CryptoPP::HashFilter(*sha1, new CryptoPP::Base32Encoder(new CryptoPP::StringSink(*hash)
				)));

		delete sha1;
	}
	std::wstring *hashWstr = new wstring((*hash).begin(), (*hash).end());

	delete hash;


	return *hashWstr;
}

