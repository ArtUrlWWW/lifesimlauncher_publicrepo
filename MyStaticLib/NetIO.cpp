#include "stdafx.h"

#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/asio.hpp>

map<wstring, FileInfo> *NetIO::localFilesForDeletion = new map<wstring, FileInfo>;
map<wstring, FileInfo> *NetIO::remoteFileForDownload = new map<wstring, FileInfo>;
int NetIO::triedTimes = 0;

using namespace std;
using namespace boost::asio;
using boost::asio::ip::tcp;

bool NetIO::GetFile(const std::string& serverName,
	const std::string& getCommand,
	std::ofstream& outFile)
{
	try {
		boost::asio::io_service io_service;

		// Get a list of endpoints corresponding to the server name.
		tcp::resolver resolver(io_service);
		tcp::resolver::query query(serverName, "http");
		tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
		tcp::resolver::iterator end;

		// Try each endpoint until we successfully establish a connection.
		tcp::socket socket(io_service);
		boost::system::error_code error = boost::asio::error::host_not_found;
		while (error && endpoint_iterator != end)
		{
			socket.close();
			socket.connect(*endpoint_iterator++, error);
		}

		boost::asio::streambuf request;
		std::ostream request_stream(&request);

		request_stream << "GET " << getCommand << " HTTP/1.0\r\n";
		request_stream << "Host: " << serverName << "\r\n";
		request_stream << "Accept: */*\r\n";
		request_stream << "Connection: close\r\n\r\n";

		// Send the request.
		boost::asio::write(socket, request);

		// Read the response status line.
		boost::asio::streambuf response;
		boost::asio::read_until(socket, response, "\r\n");

		// Check that response is OK.
		std::istream response_stream(&response);
		std::string http_version;
		response_stream >> http_version;
		unsigned int status_code;
		response_stream >> status_code;
		std::string status_message;
		std::getline(response_stream, status_message);

		// Read the response headers, which are terminated by a blank line.
		boost::asio::read_until(socket, response, "\r\n\r\n");

		// Process the response headers.
		std::string header;
		while (std::getline(response_stream, header) && header != "\r")
		{
		}

		// Write whatever content we already have to output.
		if (response.size() > 0)
		{
			outFile << &response;
		}
		// Read until EOF, writing data to output as we go.
		while (boost::asio::read(socket, response, boost::asio::transfer_at_least(1), error))
		{
			outFile << &response;
		}

	}
	catch (exception ex) {
		cout << serverName<<getCommand<<endl;
		return false;
	}
	return true;
}

//
//int main(int argc, char* argv[])
//{
//	string serverName = "demo.lizardtech.com";
//
//	string getCommand = "/lizardtech/iserv/ows?SERVICE=WMS&REQUEST=GetMap&";
//	getCommand += "LAYERS=LACounty,&STYLES=&";
//	getCommand += "BBOX=314980.5,3624089.5,443200.5,3861209.5&";
//	getCommand += "SRS=EPSG:26911&FORMAT=image/gif&HEIGHT=300&WIDTH=600";
//
//	std::ofstream outFile("image.gif", std::ofstream::out, std::ofstream::binary);
//
//	GetFile(serverName, getCommand, outFile);
//
//	outFile.close();
//
//	return 0;
//}


static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
	((std::string*)userp)->append((char*)contents, size * nmemb);
	return size * nmemb;
}

wstring NetIO::getUrlContent(string URL, bool firstAttempt) {

	CURL *curl;
	string *s = new string("");

	curl_global_init(CURL_GLOBAL_DEFAULT);

	curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, URL.c_str());
		//curl_easy_setopt(curl, CURLOPT_URL, "http://tatarstan.shop.megafon.ru/");
		//curl_easy_setopt(curl, CURLOPT_URL, "https://lifesim.biz/");

		//curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		//curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
		curl_easy_setopt(curl, CURLOPT_CAINFO, "lifesim.biz.crt.cer");

		curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT_MS, 5000L);
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, s);
		CURLcode res = curl_easy_perform(curl);
		if (res) {
			int x = 0;
			std::cout << "QQQQQQQ" << std::endl;
		}
		else {
			int x = 1;
			std::cout << "WWWWWWWWWWW" << std::endl;
		}
		curl_easy_cleanup(curl);

	}

	curl_global_cleanup();

	wstring outContent = MyStrings::StringToWString(*s);

	int outContentLength = outContent.length();

	if (outContentLength < 5 && NetIO::triedTimes < 3) {
		NetIO::triedTimes = NetIO::triedTimes + 1;
		outContent = NetIO::getUrlContent(URL, false);
	}

	//if (outContent.length() < 5 && firstAttempt) {
		//	MessageBox(nullptr,  L"Please, check your Internet connection and try again later.", L"Can't access update server", MB_OK);
	//}

	return outContent;
}


bool NetIO::readRemoteFileListXml(string serverHost, ResultArrays *ras) {
	bool error = false;

	wstring *pathToXmlFile = new wstring(L"remoteFileList.xml");
	pugi::xml_document doc2;
	wstring mainXmlText = NetIO::getUrlContent(serverHost + MyStaticLib::pathToFilesOnServerStr + "remoteFileList.xml", true);

	if ((mainXmlText).length() > 5) {
		FileIO::WriteWstringToFile(&mainXmlText, pathToXmlFile, false);

		// Load file to pugi xml object
		pugi::xml_parse_result result = doc2.load_file((*pathToXmlFile).c_str());

		// Find remote distrib version
		pugi::xml_node root2 = doc2.child(L"UpdateInfo").child(L"FileList");

		pugi::xml_node buildVersionNode = doc2.child(L"UpdateInfo").child(L"BuildInfo").child(L"BuildVersion");
		MyStaticLib::remoteDistribVersion = buildVersionNode.text().as_string();

		pugi::xml_node firstChild = (root2.first_child());

		do {
			if (firstChild != nullptr) {
				wstring pathToFile = firstChild.child(L"FilePath").text().as_string();

				boost::replace_all(pathToFile, L"\\\\", L"\\");
				boost::replace_all(pathToFile, L"\\", L"/");

				wstring fileHash = firstChild.child(L"FileHash").text().as_string();

				FileInfo *fi = new FileInfo;
				(*fi).fileHash = fileHash;
				(*fi).filePathOnServer = MyStaticLib::pathToFilesOnServer +
					MyStaticLib::remoteDistribVersion +
					L"/" +
					pathToFile;
				;
				if (pathToFile.substr(0, 1).compare(L"/") == 0) {
					pathToFile = pathToFile.substr(1, pathToFile.length() - 1);
				}
				(*fi).filePathLocal = L"LifeSimGame/" + pathToFile;
				(*fi).fileSize = firstChild.child(L"FileSize").text().as_llong();

				(*Hasher::remoteXmlListHashToFile)[fileHash+L" - "+ L"LifeSimGame/" + pathToFile] = *fi;
//				(*Hasher::remoteXmlListFileToHash)[L"LifeSimGame/" + pathToFile] = *fi;
				delete fi;
			}

		} while ((firstChild = firstChild.next_sibling()) != nullptr);
	}
	else {
		error = true;
	}

	delete pathToXmlFile;

	return error;

}