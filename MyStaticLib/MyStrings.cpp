#include "stdafx.h"

wstring MyStrings::StringToWString(string s)
{
	wstring *ws = new wstring((s).begin(), (s).end());
	return *ws;
}

void MyStrings::StringToWStringOld(string *s, wstring *ws)
{
	delete ws;
	ws = new wstring((*s).begin(), (*s).end());
	delete s;
}

wchar_t* MyStrings::Utf8ToUnicode(string *inStr) {
	int wchars_num = MultiByteToWideChar(CP_UTF8, 0, (*inStr).c_str(), -1, NULL, 0);
	wchar_t* wstr = new wchar_t[wchars_num];
	MultiByteToWideChar(CP_UTF8, 0, (*inStr).c_str(), -1, wstr, wchars_num);

	return wstr;
}