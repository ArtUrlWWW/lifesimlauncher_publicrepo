#pragma once

#include <string>

using namespace std;

class Hasher {
public:
	wstring getHashOfFile(wstring *pathToFile);

	static map<wstring, FileInfo> *LocalIndexedFilesHashToFile;
	static map<wstring, FileInfo> *remoteXmlListHashToFile;
};