#include "pugixml.hpp"
#include <string>
#include "MyXML.h"
#include "stdafx.h"

pugi::xml_node* MyXML::initLocalXmlFileList(pugi::xml_document *doc) {

	// Alternatively store as shared pointer if tree shall be used for longer
	// time or multiple client calls:
	// std::shared_ptr<pugi::xml_document> spDoc = std::make_shared<pugi::xml_document>();
	// Generate XML declaration
	auto *declarationNode = (&(*doc).append_child(pugi::node_declaration));
	(*declarationNode).append_attribute(L"version") = L"1.0";
	(*declarationNode).append_attribute(L"encoding") = L"UTF-8";
	// A valid XML doc must contain a single root node of any name
	pugi::xml_node *root;

	root = new pugi::xml_node;
	*root = (*doc).append_child(L"UpdateInfo");

	return root;

	//pugi::xml_parse_result *result = (&(*doc).load_file("c:/temp/����� �����/pugixml-1.7_test1/docs/samples/tree.xml"));
	//string l1 = (*result).description();
	//string l = "Load result: " + l1 + ", mesh name: " + (*doc).child("mesh").attribute("name").value();
	//wstring *l2 = new wstring((l).begin(), (l).end());
	//MessageBox((*l2).c_str(), (*l2).c_str(), MB_OK);
}

void MyXML::localXmlFileListAddElement(pugi::xml_node *root, wstring *pathToFile, wstring *fileHash, wstring *fileSize) {

	if ((*pathToFile).find(L"localFileList.xml") == std::string::npos &&
		(*pathToFile).find(L"remoteFileList.xml") == std::string::npos) {
		auto n1 = (*root).append_child(L"FileInfo");

		n1.append_child(L"FilePath").append_child(pugi::node_pcdata).set_value((*pathToFile).c_str());
		n1.append_child(L"FileHash").append_child(pugi::node_pcdata).set_value((*fileHash).c_str());
		n1.append_child(L"FileSize").append_child(pugi::node_pcdata).set_value((*fileSize).c_str());
	}

}