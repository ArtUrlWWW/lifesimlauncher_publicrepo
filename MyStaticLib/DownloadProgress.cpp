#include "stdafx.h"
#include "DownloadProgress.h"

long long int *downloadedBytes;
long long int downloadedBytesPrev = 0;
DownloadProgress::DownloadProgress(long long int *downloadedBytesInp) {
	downloadedBytes = downloadedBytesInp;
}


HRESULT DownloadProgress::QueryInterface(const IID &, void **) {
	return E_NOINTERFACE;
}
ULONG DownloadProgress::AddRef(void) {
	return 1;
}
ULONG DownloadProgress::Release(void) {
	return 1;
}
HRESULT DownloadProgress::OnStartBinding(DWORD dwReserved, IBinding *pib) {
	return E_NOTIMPL;
}
HRESULT DownloadProgress::GetPriority(LONG *pnPriority) {
	return E_NOTIMPL;
}
HRESULT DownloadProgress::OnLowResource(DWORD reserved) {
	return S_OK;
}
HRESULT DownloadProgress::OnStopBinding(HRESULT hresult, LPCWSTR szError) {
	return E_NOTIMPL;
}
HRESULT DownloadProgress::GetBindInfo(DWORD *grfBINDF, BINDINFO *pbindinfo) {
	return E_NOTIMPL;
}
HRESULT DownloadProgress::OnDataAvailable(DWORD grfBSCF, DWORD dwSize, FORMATETC *pformatetc, STGMEDIUM *pstgmed) {
	return E_NOTIMPL;
}
HRESULT DownloadProgress::OnObjectAvailable(REFIID riid, IUnknown *punk) {
	return E_NOTIMPL;
}

HRESULT DownloadProgress::OnProgress(ULONG ulProgress, ULONG ulProgressMax, ULONG ulStatusCode, LPCWSTR szStatusText)
{
	if (ulStatusCode == 5) {
		if (ulProgress < downloadedBytesPrev) {
		//	MessageBox(nullptr, to_wstring(x).c_str(), L"QQQ!!!", MB_OK);
		}
		else {
			(*downloadedBytes) += (ulProgress - downloadedBytesPrev);
			downloadedBytesPrev = ulProgress;
		}
	}

	/*if (ulStatusCode == 5) {
		(*downloadedBytes) += (ulProgress - downloadedBytesPrev);
		downloadedBytesPrev = ulProgress - downloadedBytesPrev;
	}*/

	//MessageBox(nullptr, to_wstring(downloadedBytesPrev).c_str(), to_wstring(downloadedBytesPrev).c_str(), MB_OK);
//	wstring *res = new wstring;
	//*res = to_wstring(ulProgress) + L" of " + to_wstring(ulProgressMax);
	//MessageBox(nullptr, (*res).c_str(), (*res).c_str(), MB_OK);
//	delete res;

	//if (szStatusText) {
	//	MessageBox(nullptr, szStatusText, szStatusText, MB_OK);
	//}

	return S_OK;
}


//
//int _tmain(int argc, _TCHAR* argv[])
//{
//	DownloadProgress progress;
//	IBindStatusCallback* callback = (IBindStatusCallback*)&progress;
//	HRESULT hr = URLDownloadToFile(0,
//		L"http://sstatic.net/stackoverflow/img/sprites.png?v=3",
//		L"c:/temp/test.png", 0,
//		static_cast<IBindStatusCallback*>(&progress));
//	return 0;
//}