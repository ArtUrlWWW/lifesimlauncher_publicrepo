#include "stdafx.h"

//Boost
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

#include <windows.h>

std::ofstream outfile("c:/temp/output.txt");

long long int FileIO::removeEmptyDirs(string rootFolder) {
	long long int folderSize = 0;
	boost::replace_all(rootFolder, "\\\\", "\\");
	boost::filesystem::path folderPath(rootFolder);
	if (boost::filesystem::exists(folderPath)) {
		boost::filesystem::directory_iterator end_itr;

		for (boost::filesystem::directory_iterator dirIte(rootFolder); dirIte != end_itr; ++dirIte)
		{
			boost::filesystem::path filePath(dirIte->path());
			try {
				if (!boost::filesystem::is_directory(dirIte->status()))
				{

					folderSize = folderSize + boost::filesystem::file_size(filePath);
					outfile << folderSize << endl;
				}
				else {
						folderSize = folderSize + removeEmptyDirs(filePath.string());
				}
			}
			catch (exception& e) {
				outfile << e.what() << endl;
			}
		}
		if (folderSize == 0) {
			try {
				if (boost::filesystem::remove_all(rootFolder)) {
					//	MessageBox(nullptr, L"remove_all!", L"CREATED!", MB_OK);
				}
				else {
					//	MessageBox(nullptr, L"NOT remove_all!", L"NOT CREATED!", MB_OK);
				}
			}
			catch (boost::filesystem::filesystem_error) {
				//	MessageBox(nullptr, L"EXCEPTION!", L"NOT CREATED!", MB_OK);
			}
		}
	}
	return folderSize;
}

void FileIO::removeUnNeededFiles(ResultArrays *ras) {
	for (auto const& x : (*NetIO::localFilesForDeletion)) {
		if (x.second.filePathLocal.find(L"localFileList.xml") == std::string::npos &&
			x.second.filePathLocal.find(L"remoteFileList.xml") == std::string::npos) {

			boost::filesystem::path fileForRemove(x.second.filePathLocal);

			try {
				if (boost::filesystem::remove_all(fileForRemove)) {
					//	MessageBox(nullptr, L"remove_all!", L"CREATED!", MB_OK);
				}
				else {
					//	MessageBox(nullptr, L"NOT remove_all!", L"NOT CREATED!", MB_OK);
				}
			}
			catch (boost::filesystem::filesystem_error) {
				//	MessageBox(nullptr, L"EXCEPTION!", L"NOT CREATED!", MB_OK);
			}
		}
	}
}


void FileIO::countFilesInDir(boost::filesystem::path dirn, int *fileCounter)
{
	/*if (dirn.compare(L".") != 0) {
		boost::replace_all(dirn, L"\\\\", L"\\");
		boost::replace_all(dirn, L"\\", L"/");
	}*/
	//boost::filesystem::path folderPath(dirn);
	if (boost::filesystem::exists(dirn)) {
		boost::filesystem::directory_iterator end_itr;

		for (boost::filesystem::directory_iterator dirIte(dirn); dirIte != end_itr; ++dirIte)
		{
			try {
				if (!boost::filesystem::is_directory(dirIte->status()))
				{
					*fileCounter = *fileCounter + 1;
				}
				else {
						countFilesInDir(dirIte->path(), fileCounter);
				}
			}
			catch (exception& e) {
				outfile << e.what() << endl;
			}
		}
	}
}

void FileIO::WriteWstringToFile(wstring *stringToPrint, wstring *pathToFile, bool append) {

	std::ofstream outFile;
	if (append == true) {
		outFile = ofstream((*pathToFile), std::ios::out | std::ios::binary | ios_base::app);
	}
	else {
		outFile = ofstream((*pathToFile), std::ios::out | std::ios::binary);
	}

	outFile.write((char *)(*stringToPrint).c_str(), (*stringToPrint).length() * sizeof(wchar_t));
	outFile.close();

}


void FileIO::WriteStringToFile(string *stringToPrint, string *pathToFile, bool append) {
	ofstream *outfile = new ofstream;
	if (append) {
		(*outfile).open((*pathToFile), ios_base::app);
	}
	else {
		(*outfile).open((*pathToFile));
	}
	(*outfile) << (*stringToPrint) << endl;
	(*outfile).close();
	delete outfile;
}

long long int* FileIO::GetFileSize(WIN32_FIND_DATA *f) {

	LARGE_INTEGER *size = new LARGE_INTEGER;
	(*size).HighPart = (*f).nFileSizeHigh;
	(*size).LowPart = (*f).nFileSizeLow;

	long long int *out = new long long int;
	*out = (*size).QuadPart;
	delete size;

	return out;
}

void FileIO::hashFilesInDir(boost::filesystem::path dirn, pugi::xml_node *root,
	void(*calcHashFunction)(wstring, wstring, uintmax_t, pugi::xml_node*))
{
	try {

		long long int folderSize = 0;
		if (boost::filesystem::exists(dirn)) {
			boost::filesystem::directory_iterator end_itr;

			for (boost::filesystem::directory_iterator dirIte(dirn); dirIte != end_itr; ++dirIte)
			{
				boost::filesystem::path filePath(dirIte->path());
				try {
					if (boost::filesystem::is_directory(dirIte->status()))
					{
							hashFilesInDir(dirIte->path(), root, calcHashFunction);
					}
					else {
							try {
								boost::this_thread::interruption_point();
								wstring s1 = dirIte->path().wstring();
								wstring s2 = MyStrings::StringToWString(to_string(boost::filesystem::file_size(dirIte->path())));
									(*calcHashFunction)(
									s1,
									s2,
									boost::filesystem::file_size(dirIte->path()),
									root
									);
							}
							catch (exception ex) {
								MessageBox(nullptr, L"Some error occured on file hashing2.", L"Error:", MB_OK);
							}
					}
				}
				catch (exception& e) {
					outfile << e.what() << endl;
				}
			}

		}
	}
	catch (...) {
		MessageBox(nullptr, L"Some error occured on file indexing.", L"Error:", MB_OK);
	}

}