#pragma once
#include "stdafx.h"

class MyHasherFileListGenerator {
public:

	static void calcHash(wstring pathToFile, wstring fileSizeWstr, uintmax_t fileSize,
		pugi::xml_node *root);

	static wstring pathToMainDir;
};
