// MyHasherFileListGenerator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>
#include <clocale>
#include <time.h>
#include <boost/algorithm/string.hpp>

#define  _CRT_SECURE_NO_WARNINGS

wstring MyHasherFileListGenerator::pathToMainDir = L"e:\\Package2\\WindowsNoEditor\\";

void MyHasherFileListGenerator::calcHash(wstring pathToFile, wstring fileSizeWstr, uintmax_t fileSize,
	pugi::xml_node *root)
{

	Hasher * hasher = new Hasher();

	wstring  hashWstr = hasher->getHashOfFile(&pathToFile);
	//std::wcout << hashWstr << std::endl;

	boost::replace_all(pathToFile, L"\\\\", L"\\");
	boost::replace_all(pathToFile, L"\\", L"/");
	
	//hashWstr = hashWstr + L" - " + pathToFile;

	boost::replace_all(pathToFile, MyHasherFileListGenerator::pathToMainDir, L"");
	pathToFile = pathToFile + L".png";
	
	MyXML::localXmlFileListAddElement(root, &pathToFile, &hashWstr, &fileSizeWstr);

	delete hasher;

}

const std::string currentDateTime() {
	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[80];
	tstruct = *localtime(&now);
	// Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
	// for more information about date/time format
	strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

	return buf;
}


int main()
{
	boost::replace_all(MyHasherFileListGenerator::pathToMainDir, L"\\\\", L"\\");
	boost::replace_all(MyHasherFileListGenerator::pathToMainDir, L"\\", L"/");

	pugi::xml_document *doc;
	doc = new pugi::xml_document;
	pugi::xml_node *root = MyXML::initLocalXmlFileList(doc);

	pugi::xml_node FileListRoot = (*root).append_child(L"FileList");

	FileIO::hashFilesInDir(MyHasherFileListGenerator::pathToMainDir, &FileListRoot, &MyHasherFileListGenerator::calcHash);

	string currentTime = currentDateTime();

	auto n1 = (*root).append_child(L"BuildInfo");
	n1.append_child(L"BuildVersion").append_child(pugi::node_pcdata)
		.set_value((MyStrings::StringToWString(currentTime)).c_str());

	// Save XML tree to file.
	(*doc).save_file(L"e:\\remoteFileList.xml");

    return 0;
}

