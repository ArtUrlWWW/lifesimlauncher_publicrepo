
// LifeSimLauncherDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LifeSimLauncher.h"
#include "LifeSimLauncherDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CStatic* CLifeSimLauncherDlg::currentWorkStat = nullptr;
CStatic* CLifeSimLauncherDlg::downloadingBytesCur = nullptr;
pugi::xml_document *CLifeSimLauncherDlg::doc = new pugi::xml_document;
CProgressCtrl *CLifeSimLauncherDlg::cpr;
CButton *CLifeSimLauncherDlg::cbtn;

boost::thread *timerThread;
boost::thread *hashFilesTh;

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CLifeSimLauncherDlg dialog
CLifeSimLauncherDlg::CLifeSimLauncherDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_LIFESIMLAUNCHER_DIALOG, pParent)
{
	//m_hIcon = AfxGetApp()->LoadIcon(132);
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CLifeSimLauncherDlg::~CLifeSimLauncherDlg() {
	//MyStaticLib::isProgramClosingNow = true;
	//timerThread->interrupt();
	//hashFilesTh->interrupt();

}

void CLifeSimLauncherDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CLifeSimLauncherDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CLifeSimLauncherDlg::OnBnClickedButton1)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON2, &CLifeSimLauncherDlg::OnBnClickedButton2)
END_MESSAGE_MAP()



// CLifeSimLauncherDlg message handlers

BOOL CLifeSimLauncherDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	cpr = (CProgressCtrl*)this->GetDlgItem(IDC_PROGRESS1);
	cpr->SetRange(0, 100);
	cpr->SetStep(1);

	ce = (CEdit*)this->GetDlgItem(IDC_EDIT1);
	cbtn = (CButton*)this->GetDlgItem(IDC_BUTTON1);
	CLifeSimLauncherDlg::currentWorkStat = (CStatic *)this->GetDlgItem(IDC_STATIC2);
	CLifeSimLauncherDlg::downloadingBytesCur = (CStatic *)this->GetDlgItem(IDC_STATIC4);

	//setlocale(LC_ALL, "");
	locale::global(locale("Russian_Russia.1251"));

	timerThread = new boost::thread{ CLifeSimLauncherDlg::tick2 };

	OnBnClickedButton1();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CLifeSimLauncherDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLifeSimLauncherDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLifeSimLauncherDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void unpackKeys() {
	HRSRC hrsrc = FindResource(NULL, MAKEINTRESOURCE(IDR_BINARYTYPE1), _T("BINARYTYPE"));
	//FindResource(NULL, MAKEINTRESOURCE(IDR_BINARYTYPE1), RT_BITMAP);
	HGLOBAL hLoaded = LoadResource(NULL, hrsrc);
	LPVOID lpLock = LockResource(hLoaded);
	DWORD dwSize = SizeofResource(NULL, hrsrc);
	std::wstring path = TEXT("lifesim.biz.crt.cer");

	HANDLE hFile = CreateFile(path.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	DWORD dwByteWritten;
	WriteFile(hFile, lpLock, dwSize, &dwByteWritten, NULL);
	CloseHandle(hFile);
	FreeResource(hLoaded);
}

void CLifeSimLauncherDlg::hashFilesThread() {
	try {
		boost::this_thread::interruption_point();

		wstring *pathToMainDir = new wstring(L"./LifeSimGame");

		MyLib::hashFilesInDirExternal(doc, pathToMainDir);
		boost::this_thread::interruption_point();

		delete Hasher::LocalIndexedFilesHashToFile;
		delete Hasher::remoteXmlListHashToFile;
		delete MyLib::fileCounter;
		delete MyLib::fileCounterProg;
		delete pathToMainDir;

		cbtn->EnableWindow(true);
	}
	catch (...) {
	}
}

UINT CLifeSimLauncherDlg::WorkerThreadProc(void)
{

	unpackKeys();

	hashFilesTh = new boost::thread{ CLifeSimLauncherDlg::hashFilesThread };

	return 0;
}

UINT mainThreadFunct(LPVOID pParam) {

	ASSERT(pParam != NULL);
	CLifeSimLauncherDlg* pThis = reinterpret_cast<CLifeSimLauncherDlg*>(pParam);
	return pThis->WorkerThreadProc();
}

void CLifeSimLauncherDlg::OnBnClickedButton1()
{
	MyLib::fileCounter = new int(0);
	MyLib::fileCounterProg = new int(0);

	Hasher::LocalIndexedFilesHashToFile = new map<wstring, FileInfo>;
	Hasher::remoteXmlListHashToFile = new map<wstring, FileInfo>;

	doc = new pugi::xml_document;

	AfxBeginThread(mainThreadFunct, (LPVOID)this);

	cbtn->EnableWindow(false);

}

void CLifeSimLauncherDlg::OnClose()
{
	//CDialogEx::OnClose();
}


void CLifeSimLauncherDlg::OnBnClickedButton2()
{
	timerThread->interrupt();
	hashFilesTh->interrupt();
	//hashFilesTh->join();
	Sleep(500);
	CDialogEx::EndDialog(0);
}



boost::asio::io_service io_service;
boost::posix_time::seconds interval(1); // 1 second
boost::asio::deadline_timer timer(io_service, interval);

void CLifeSimLauncherDlg::tick(const boost::system::error_code& /*e*/) {
	try
	{
		CLifeSimLauncherDlg::cpr->SetPos(MyLib::currentProgress);
		
		//reinterpret_cast<CStatic*>((*ras).downloadingBytesCurrent)->SetWindowText(to_wstring(MyLib::currentThreadCount).c_str());

		wstring* statusText = new wstring();
		if (NetIO::triedTimes < 1) {
			*statusText = L"Downloading files in " + to_wstring(MyLib::currentThreadCount) + L" threads";
		}
		else {
			if (MyLib::currentThreadCount < 1) {
				*statusText = L"There are " + to_wstring(NetIO::triedTimes) + L" files were downloaded with errors. Please, restart LifeSimLauncher to fix errros.";
			}
		}

		CLifeSimLauncherDlg::downloadingBytesCur->SetWindowText((*statusText).c_str());

		// Reschedule the timer for 1 second in the future:
		timer.expires_at(timer.expires_at() + interval);
		// Posts the timer event
		timer.async_wait(tick);
	}
	catch (boost::thread_interrupted&) {

	}
}

void CLifeSimLauncherDlg::tick2() {
	try
	{
		/*while (true) {
		try
		{
		boost::this_thread::interruption_point();
		this_thread::sleep_for(chrono::milliseconds(200));
		}
		catch (...) {
		break;
		}

		}*/

		boost::this_thread::interruption_point();

		// Schedule the timer for the first time:
		timer.async_wait(tick);
		// Enter IO loop. The timer will fire for the first time 1 second from now:
		io_service.run();
	}
	catch (...) {

	}
}