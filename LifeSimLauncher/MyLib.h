#pragma once

#include "stdafx.h"

//Boost
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;



class MyLib {
public:
	static void processFileLists();
	static void calcHash(wstring pathToFile, wstring fileSizeWstr, uintmax_t fileSize, pugi::xml_node *root);

	static void hashFilesInDirExternal(pugi::xml_document *doc, wstring *dirn);

	//typedef basic_istringstream<wchar_t> wistringstream;
	static void downloadFile(FileInfo fi, int xInp);
	static void downloadAllFiles();


	static int *fileCounter;
	static int *fileCounterProg;
	static int currentThreadCount;
	static int currentProgress;


private:



};