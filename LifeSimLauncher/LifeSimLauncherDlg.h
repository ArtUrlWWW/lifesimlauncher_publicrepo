
// LifeSimLauncherDlg.h : header file
//

#pragma once

#include "MyLib.h"

#define PUGIXML_WCHAR_MODE

using namespace std;

// CLifeSimLauncherDlg dialog
class CLifeSimLauncherDlg : public CDialogEx
{
	// Construction
public:
	CLifeSimLauncherDlg(CWnd* pParent = NULL);	// standard constructor
	~CLifeSimLauncherDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_LIFESIMLAUNCHER_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:

	afx_msg void OnBnClickedButton1();
	UINT WorkerThreadProc(void);

	static pugi::xml_document *doc;

	static CButton  *cbtn;
	static CStatic *currentWorkStat;
	static CStatic *downloadingBytesCur;
	static CProgressCtrl * cpr;

	static void hashFilesThread();
private:
	
	CEdit *ce;



public:

	afx_msg void OnClose();
	afx_msg void OnBnClickedButton2();

	static void tick(const boost::system::error_code& /*e*/);
	static void tick2();
};
