#include "stdafx.h"
#include "MyLib.h"
//#include "DownloadProgress.h"
#include "LifeSimLauncherDlg.h"

ResultArrays *ras;
// http://files.lifesim.biz/builds/stable/Windows_x32/2016-07-19.20:03:50/
string serverHost = "http://files.lifesim.biz/";

int* MyLib::fileCounter = new int(0);
int* MyLib::fileCounterProg = new int(0);
int MyLib::currentThreadCount = 0;
int MyLib::currentProgress = 0;


void MyLib::processFileLists() {
	// Let's find local files, that pending for deletion

	(*ras).bytesToBeDownloaded = 0;
	NetIO::localFilesForDeletion = new map<wstring, FileInfo>;
	for (auto const& x : (*Hasher::LocalIndexedFilesHashToFile)) {
		map<wstring, FileInfo>::iterator foundPos = (*Hasher::remoteXmlListHashToFile).find(x.first);
		if (foundPos == (*Hasher::remoteXmlListHashToFile).end()) {
			// not found
			(*NetIO::localFilesForDeletion)[x.second.filePathLocal.c_str()] = x.second;
		}
	}

	// Let's find remote files, that pending for download
	NetIO::remoteFileForDownload = new map<wstring, FileInfo>;

	for (auto const& x : (*Hasher::remoteXmlListHashToFile)) {

		if ((*Hasher::LocalIndexedFilesHashToFile).find(x.first) == (*Hasher::LocalIndexedFilesHashToFile).end()) {
			// not found
			(*NetIO::remoteFileForDownload)[x.second.filePathOnServer.c_str()] = x.second;
			(*ras).bytesToBeDownloaded += x.second.fileSize;
		}
		else {
			// found
		}
	}

	FileIO::removeUnNeededFiles(ras);
	FileIO::removeEmptyDirs(".");
	downloadAllFiles();

	delete NetIO::localFilesForDeletion;
	delete NetIO::remoteFileForDownload;

}

void MyLib::downloadFile(FileInfo fi, int xInp) {
	MyLib::currentThreadCount++;
	wstring url = MyStrings::StringToWString(serverHost);
	url = url + L"/" + fi.filePathOnServer;

	wstring fis = fi.filePathLocal;

	if (fis.find_last_of(L"/") != wstring::npos && fis.find_last_of(L"/") > 2) {
		wstring fisD = fis.substr(0, fis.find_last_of(L"/"));
		boost::filesystem::path dir(fisD);

		try {
			boost::filesystem::create_directories(dir);
		}
		catch (boost::filesystem::filesystem_error) {
			//	MessageBox(nullptr, L"EXCEPTION!", L"NOT CREATED!", MB_OK);
		}
	}

	fis = fis.substr(0, fis.length() - 4);

	string serverHostLocal = serverHost;
	boost::replace_all(serverHostLocal, "http://", "");
	boost::replace_all(serverHostLocal, "https://", "");
	boost::replace_all(serverHostLocal, "/", "");

	boost::replace_all(url, MyStrings::StringToWString(serverHost), L"");

	std::ofstream outFile(fis, std::ofstream::out | std::ofstream::binary);

	const std::string url2(url.begin(), url.end());

	if (!NetIO::GetFile(serverHostLocal, url2, outFile)) {
		NetIO::triedTimes = NetIO::triedTimes + 1;
	}

	outFile.close();

	(*ras).bytesAlreadyDownloaded += fi.fileSize;

	long long int x = 0;

	currentProgress = ((*ras).bytesAlreadyDownloaded * 100) / (*ras).bytesToBeDownloaded;

	MyLib::currentThreadCount--;

}


struct thread_pool {
	typedef std::unique_ptr<boost::asio::io_service::work> asio_worker;

	thread_pool(int threads) :service(), service_worker(new asio_worker::element_type(service)) {
		for (int i = 0; i < threads; ++i) {
			auto worker = [this] { return service.run(); };
			grp.add_thread(new boost::thread(worker));
		}
	}

	template<class F>
	void enqueue(F f) {
		service.post(f);
	}

	~thread_pool() {
		service_worker.reset();
		grp.join_all();
		service.stop();
	}

private:
	boost::asio::io_service service;
	asio_worker service_worker;
	boost::thread_group grp;
};


void MyLib::downloadAllFiles() {

	thread_pool * pool = new thread_pool(30);

	int x = 0;
	for (auto const& fi : (*NetIO::remoteFileForDownload)) {
		x++;
		/*
		* This will assign tasks to the thread pool.
		* More about boost::bind: "http://www.boost.org/doc/libs/1_54_0/libs/bind/bind.html#with_functions"
		*/
		(*pool).enqueue(boost::bind(downloadFile, fi.second, x));
	}

	delete pool;

	if (NetIO::triedTimes > 0) {
		MessageBox(nullptr, L"Your files are not updated. Please, check your Internet Connection and restart Launcher to update files.", L"Connection error", MB_OK);
	}
	else {
		currentProgress = 0;
		MessageBox(nullptr, L"Update is done", L"Update is done", MB_OK);
	}

}

void MyLib::hashFilesInDirExternal(pugi::xml_document *doc, wstring *dirn	) {

	ras = new ResultArrays;

	pugi::xml_node *root = MyXML::initLocalXmlFileList(doc);

	(*ras).bytesAlreadyDownloaded = 0;
	(*ras).downloadedBytes = new long long int(0);
	(*(*ras).downloadedBytes) = 0;

	bool connectionError = NetIO::readRemoteFileListXml(serverHost, ras);
	//connectionError = true;
	if (!connectionError) {
		//reinterpret_cast<CStatic*>((*ras).currentWorkStatus)->SetWindowTextW(L"Step 1 of 3:Counting files");
		CLifeSimLauncherDlg::currentWorkStat->SetWindowTextW(L"Step 1 of 3:Counting local game files");
		FileIO::countFilesInDir(*dirn, MyLib::fileCounter);
		//reinterpret_cast<CStatic*>((*ras).currentWorkStatus)->SetWindowTextW(L"Step 2 of 3:Indexing files");
		CLifeSimLauncherDlg::currentWorkStat->SetWindowTextW(L"Step 2 of 3:Indexing local game files");

		pugi::xml_node FileListRoot = (*root).append_child(L"FileList");

		try
		{
			boost::this_thread::interruption_point();

			FileIO::hashFilesInDir(*dirn, &FileListRoot, &MyLib::calcHash);
		}
		catch (...) {

		}


		// Save XML tree to file.
		(*doc).save_file(L"localFileList.xml");

		//reinterpret_cast<CStatic*>((*ras).currentWorkStatus)->SetWindowTextW(L"Step 3 of 3:Updating local files");
		CLifeSimLauncherDlg::currentWorkStat->SetWindowTextW(L"Step 3 of 3:Updating local files");
		processFileLists();
		CLifeSimLauncherDlg::currentWorkStat->SetWindowTextW(L"Update completed.");

		delete doc;
		delete ras;
	}
	else {
		MessageBox(nullptr, L"Connection error. Please, check your Internet Connection", L"Connection error", MB_OK);
	}

	delete root;
}

void MyLib::calcHash(wstring pathToFile, wstring fileSizeWstr, uintmax_t fileSize,
	pugi::xml_node *root)
{
	try {
		boost::this_thread::interruption_point();

		Hasher* hasher = new Hasher();
		wstring  fileHash = hasher->getHashOfFile(&pathToFile);  
		//Sleep(100);
		boost::replace_all(pathToFile, L"\\\\", L"/");
		boost::replace_all(pathToFile, L"\\", L"/");

		if (boost::starts_with(pathToFile, "./")) {
			pathToFile = pathToFile.substr(2, pathToFile.length() - 2);
		}
	
		FileInfo fi;
		fi.fileHash = fileHash;
		fi.filePathLocal = pathToFile;
		fi.fileSize = fileSize;
		(*Hasher::LocalIndexedFilesHashToFile)[fileHash + L" - " + pathToFile + L".png"] = fi;

		MyXML::localXmlFileListAddElement(root, &pathToFile, &fileHash, &fileSizeWstr);
		
		(*MyLib::fileCounterProg)++;
		MyLib::currentProgress = ((*MyLib::fileCounterProg) * 100 / (*MyLib::fileCounter));
		/*
			cpr->SetPos(*currentProgress);
			*/
		delete hasher;

		//Sleep(100);
	}
	catch (exception ex) {
		string z1(ex.what());
		wstring z11 = MyStrings::StringToWString(z1);

		MessageBox(nullptr, z11.c_str(), L"Error:", MB_OK);
	}

}

void getFileSize() {
	//ifstream myFileStream("e:/MyGame/Source/MyGame/FurnitureLib.cpp", ifstream::ate | ifstream::binary);
	//wstring name = to_wstring(myFileStream.tellg());
}